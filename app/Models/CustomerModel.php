<?php

namespace App\Models;
use CodeIgniter\Model;

class CustomerModel extends Model{
    public function getcustomer()
    { 
        $db = \Config\Database::connect();
        $query = $db->query('Select cu.c_id,cu.name,cu.email,cu.registration_date,count(oh.o_id) as ordernumber from customers cu LEFT JOIN order_history oh on cu.c_id = oh.customer_id GROUP BY cu.c_id HAVING COUNT(oh.o_id) > 0;
        ');
        $result = $query->getResult('array');
        return $result;
    }
    public function getorderhistory(){
        $db = \Config\Database::connect();
        $query = $db->query('Select * from order_history');
        $result = $query->getResult('array');
        return $result;
    }
        
}

?>