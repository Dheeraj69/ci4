<?php

namespace App\Controllers;

class Home extends BaseController
{
   
    public function index()
    {
        $customerModel = new \App\Models\CustomerModel();
        $data['customers'] = $customerModel->getcustomer();
        $data['orders'] = $customerModel->getorderhistory();
        // echo '<pre>'; print_r($data); die;
        // $data = $data['name'];
        return view('welcome_message', $data);
    }
}
